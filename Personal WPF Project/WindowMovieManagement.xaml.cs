﻿using FluentValidation;
using MovieLibrary.DTO;
using MovieLibrary.IServices;
using System;
using System.Windows;

namespace Personal_WPF_Project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class WindowMovieManagement : Window
    {
        private readonly IMovieService _movieService;
        private readonly IReviewService _reviewService;
        private readonly IAccountService _accountService;
        private readonly IValidator<MovieDTO> _movieValidator;
        private readonly IValidator<ReviewDTO> _reviewValidator;

        public WindowMovieManagement(IMovieService movieService,
                                     IReviewService reviewService,
                                     IAccountService accountService,
                                     IValidator<MovieDTO> movieValidator,
                                     IValidator<ReviewDTO> reviewValidator)
        {
            InitializeComponent();
            _movieService = movieService;
            _reviewService = reviewService;
            _accountService = accountService;
            _movieValidator = movieValidator;
            _reviewValidator = reviewValidator;
        }

        private MovieDTO GetMovieObject()
        {
            MovieDTO movie = null;
            try
            {
                movie = new MovieDTO
                {
                    Name = txtMovieName.Text,
                    Director = txtDirector.Text,
                    Rating = double.Parse(txtRating.Text),
                    Description = txtDescription.Text
                };
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Get movie");
            }
            return movie;
        }

        public async void LoadMovieList()
        {
            lvMovies.ItemsSource = await _movieService.GetAllMovies();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadMovieList();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Load movie list");
            }
        }

        private async void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MovieDTO movie = GetMovieObject();
                var validate = _movieValidator.Validate(movie);
                if (validate.IsValid)
                {
                    await _movieService.AddMovieAsync(movie);
                    LoadMovieList();
                    MessageBox.Show($"{movie.Name} inserted successfully", "insert movie");
                }
                else
                {
                    MessageBox.Show("Insert movie fail!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Insert movie");
            }
        }

        private async void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var id = int.Parse(txtMovieId.Text);
                MovieDTO movie = GetMovieObject();
                var validate = _movieValidator.Validate(movie);
                if (validate.IsValid)
                {
                    await _movieService.UpdateMovieAsync(id, movie);
                    LoadMovieList();
                    MessageBox.Show($"{movie.Name} updated successfully", "update movie");
                }
                else
                {
                    MessageBox.Show("Update movie fail!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Update movie");
            }
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var id = int.Parse(txtMovieId.Text);
                MovieDTO movie = GetMovieObject();
                MessageBoxResult result = MessageBox.Show($"Do you want to delete movie {id}", "Confirmation", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    await _movieService.DeleteMovieAsync(id);
                    LoadMovieList();
                    MessageBox.Show($"{movie.Name} deleted successfully", "Delete movie");
                }
                else
                {
                    MessageBox.Show("Delete movie fail!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Delete movie");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WindowReviewManagement windowReviewManagement = new WindowReviewManagement(_reviewService, _movieService, _accountService, _movieValidator, _reviewValidator);
            windowReviewManagement.Show();
            Close();
        }

        private async void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var name = txtSearch.Text;
                lvMovies.ItemsSource = await _movieService.GetMoviesByName(name);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Searching");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Login login = new Login(_accountService, _movieService, _reviewService, _movieValidator, _reviewValidator);
            login.Show();
            Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
