﻿using FluentValidation;
using MovieLibrary.DTO;
using MovieLibrary.Entities;
using MovieLibrary.IServices;
using MovieLibrary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Personal_WPF_Project
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        private readonly IAccountService _accountService;
        private readonly IMovieService _movieService;
        private readonly IReviewService _reviewService;
        private readonly IValidator<MovieDTO> _movieValidator;
        private readonly IValidator<ReviewDTO> _reviewValidator;

        public Register(IAccountService accountService,
                        IMovieService movieService,
                        IReviewService reviewService,
                        IValidator<MovieDTO> movieValidator,
                        IValidator<ReviewDTO> reviewValidator)
        {
            InitializeComponent();
            _accountService = accountService;
            _movieService = movieService;
            _reviewService = reviewService;
            _movieValidator = movieValidator;
            _reviewValidator = reviewValidator;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var username = txtUsername.Text;
                var password = txtPassword.Password;
                var getAccount = await _accountService.GetAccountByUsername(username);
                if (getAccount is null)
                {
                    AccountDTO accountDTO = new AccountDTO
                    {
                        Username = username,
                        Password = password,
                    };
                    await _accountService.AddAccountAync(accountDTO);
                    MessageBox.Show($"{accountDTO.Username} inserted successfully", "register");
                    Login login = new Login(_accountService, _movieService, _reviewService, _movieValidator, _reviewValidator);
                    login.Show();
                    Close();
                }
                else
                {
                    MessageBox.Show("Username or password is already existed!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Register");
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Login login = new Login(_accountService, _movieService, _reviewService, _movieValidator, _reviewValidator);
            login.Show();
            Close();
        }
    }
}
