﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieLibrary;
using MovieLibrary.DTO;
using MovieLibrary.IRepositories;
using MovieLibrary.IServices;
using MovieLibrary.Repositories;
using MovieLibrary.Services;
using MovieLibrary.Validations;
using System;
using System.IO;
using System.Windows;

namespace Personal_WPF_Project
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ServiceProvider serviceProvider;

        public App()
        {
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            serviceProvider = services.BuildServiceProvider();
        }
        private void ConfigureServices(IServiceCollection services)
        {
            // Add services to the container.
            IConfiguration configuration = new ConfigurationBuilder()
                                               .SetBasePath(Directory.GetCurrentDirectory())
                                               .AddJsonFile("appsettings.json").Build();
            services.AddSingleton(typeof(IMovieService), typeof(MovieService));
            services.AddSingleton(typeof(IReviewService), typeof(ReviewService));
            services.AddSingleton(typeof(IAccountService), typeof(AccountService));
            services.AddSingleton(typeof(IMovieRepository), typeof(MovieRepository));
            services.AddSingleton(typeof(IReviewRepository), typeof(ReviewRepository));
            services.AddSingleton(typeof(IAccountRepository), typeof(AccountRepository));

            //add validation
            services.AddSingleton<IValidator<AccountDTO>, AccountValidation>();
            services.AddSingleton<IValidator<MovieDTO>, MovieValidation>();
            services.AddSingleton<IValidator<ReviewDTO>, ReviewValidation>();


            services.AddSingleton(typeof(IUnitOfWork), typeof(UnitOfWork));
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("data"));
            });
            services.AddSingleton<WindowMovieManagement>();
            services.AddSingleton<WindowReviewManagement>();
            services.AddSingleton<Login>();
        }

        private void OnStartup(object sender, StartupEventArgs e)
        {
            var mainWindow = serviceProvider.GetService<Login>();
            mainWindow.Show();
        }

    }
}
