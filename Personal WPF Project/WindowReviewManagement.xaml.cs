﻿using FluentValidation;
using MovieLibrary.DTO;
using MovieLibrary.IServices;
using System;
using System.Windows;

namespace Personal_WPF_Project
{
    /// <summary>
    /// Interaction logic for WindowReviewManagement.xaml
    /// </summary>
    public partial class WindowReviewManagement : Window
    {
        private readonly IReviewService _reviewService;
        private readonly IMovieService _movieService;
        private readonly IAccountService _accountService;
        private readonly IValidator<MovieDTO> _movieValidator;
        private readonly IValidator<ReviewDTO> _reviewValidator;

        public WindowReviewManagement(IReviewService reviewService,
                                      IMovieService movieService,
                                      IAccountService accountService,
                                      IValidator<MovieDTO> movieValidator,
                                      IValidator<ReviewDTO> reviewValidator)
        {
            InitializeComponent();
            _reviewService = reviewService;
            _movieService = movieService;
            _accountService = accountService;
            _movieValidator = movieValidator;
            _reviewValidator = reviewValidator;
        }

        private ReviewDTO GetReviewObject()
        {
            ReviewDTO reviewDTO = null;
            try
            {
                reviewDTO = new ReviewDTO
                {
                    Title = txtTitle.Text,
                    Content = txtContent.Text,
                    Rating = double.Parse(txtRating.Text),
                    MovieId = int.Parse(txtMovieId.Text)
                };
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return reviewDTO;
        }

        public async void LoadReviewList()
        {
            lvReviews.ItemsSource = await _reviewService.GetAllReviewsAsync();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadReviewList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load review list");
            }
        }

        private async void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReviewDTO reviewDTO = GetReviewObject();
                var validate = _reviewValidator.Validate(reviewDTO);
                if (validate.IsValid)
                {
                    await _reviewService.AddReviewAsync(reviewDTO);
                    LoadReviewList();
                    MessageBox.Show($"Inserted successfully", "insert review");
                }
                else
                {
                    MessageBox.Show("Insert review fail!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Insert review");
            }
        }

        private async void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var id = int.Parse(txtReviewId.Text);
                ReviewDTO reviewDTO = GetReviewObject();
                var validate = _reviewValidator.Validate(reviewDTO);
                if (validate.IsValid)
                {
                    await _reviewService.UpdateReviewAsync(id, reviewDTO);
                    LoadReviewList();
                    MessageBox.Show($"Updated successfully", "update review");
                }
                else
                {
                    MessageBox.Show("Update review fail!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Update review");
            }
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var id = int.Parse(txtReviewId.Text);
                ReviewDTO reviewDTO = GetReviewObject();
                MessageBoxResult result = MessageBox.Show($"Do you want to delete review {id}", "Confirmation", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    await _reviewService.DeleteReviewAsync(id);
                    LoadReviewList();
                    MessageBox.Show($"Deleted successfully", "Delete review");
                }
                else
                {
                    MessageBox.Show("Delete review fail!");
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "delete review");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WindowMovieManagement windowMovieManagement = new WindowMovieManagement(_movieService, _reviewService, _accountService, _movieValidator, _reviewValidator);
            windowMovieManagement.Show();
            Close();
        }

        private async void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var title = txtSearch.Text;
                lvReviews.ItemsSource = await _reviewService.GetReviewsByTitle(title);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Searching");
            }
        }
    }
}
