﻿using FluentValidation;
using MovieLibrary.DTO;
using MovieLibrary.IServices;
using System;
using System.Windows;

namespace Personal_WPF_Project
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        private readonly IAccountService _accountService;
        private readonly IMovieService _movieService;
        private readonly IReviewService _reviewService;
        private readonly IValidator<MovieDTO> _movieValidator;
        private readonly IValidator<ReviewDTO> _reviewValidator;

        public Login(IAccountService accountService,
                     IMovieService movieService,
                     IReviewService reviewService,
                     IValidator<MovieDTO> movieValidator,
                     IValidator<ReviewDTO> reviewValidator)
        {
            InitializeComponent();
            _accountService = accountService;
            _movieService = movieService;
            _reviewService = reviewService;
            _movieValidator = movieValidator;
            _reviewValidator = reviewValidator;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var username = txtUsername.Text;
                var password = txtPassword.Password;
                var login = await _accountService.GetAccount(username, password);
                if (login is null)
                {
                    MessageBox.Show("Inval username/password");
                }
                else
                {
                    WindowMovieManagement windowMovieManagement = new WindowMovieManagement(_movieService, _reviewService, _accountService, _movieValidator, _reviewValidator);
                    windowMovieManagement.Show();
                    Close();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Login");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Register register = new Register(_accountService, _movieService, _reviewService, _movieValidator, _reviewValidator);
            register.Show();
            Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
