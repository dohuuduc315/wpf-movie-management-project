﻿using MovieLibrary.DTO;
using MovieLibrary.Entities;

namespace MovieLibrary.IServices
{
    public interface IAccountService
    {
        Task<AccountDTO> AddAccountAync(AccountDTO accountDTO);
        Task<Account> GetAccount(string username, string password);
        Task<Account> GetAccountByUsername(string username);
    }
}
