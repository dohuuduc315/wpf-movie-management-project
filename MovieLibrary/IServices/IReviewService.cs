﻿using MovieLibrary.DTO;
using MovieLibrary.Entities;

namespace MovieLibrary.IServices
{
    public interface IReviewService
    {
        Task<ReviewDTO> AddReviewAsync(ReviewDTO reviewDTO);
        Task<ReviewDTO> UpdateReviewAsync(int id, ReviewDTO reviewDTO);
        Task DeleteReviewAsync(int id);
        Task<Review> GetReviewByIdAsync(int id);
        Task<List<Review>> GetAllReviewsAsync();
        Task<List<Review>> GetReviewsByTitle(string title);
    }
}
