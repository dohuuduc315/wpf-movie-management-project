﻿using MovieLibrary.DTO;
using MovieLibrary.Entities;

namespace MovieLibrary.IServices
{
    public interface IMovieService
    {
        Task<MovieDTO> AddMovieAsync(MovieDTO movieDTO);
        Task<MovieDTO> UpdateMovieAsync(int id, MovieDTO movieDTO);
        Task DeleteMovieAsync(int id);
        Task<List<Movie>> GetAllMovies();
        Task<Movie> GetMovieByIdAsync(int id);
        Task<List<Movie>> GetMoviesByName(string name);
    }
}
