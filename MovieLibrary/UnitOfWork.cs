﻿using MovieLibrary.IRepositories;
using MovieLibrary.Repositories;

namespace MovieLibrary
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IMovieRepository _movieRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IAccountRepository _accountRepository;

        public UnitOfWork(AppDbContext context,
                          IMovieRepository movieRepository,
                          IReviewRepository reviewRepository,
                          IAccountRepository accountRepository)
        {
            _context = context;
            _movieRepository = movieRepository;
            _reviewRepository = reviewRepository;
            _accountRepository = accountRepository;
        }
        public IMovieRepository MovieRepository => _movieRepository;

        public IReviewRepository ReviewRepository => _reviewRepository;

        public IAccountRepository AccountRepository => _accountRepository;

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<int> SaveChangeAsync() => await _context.SaveChangesAsync();
    }
}
