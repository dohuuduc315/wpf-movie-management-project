﻿using FluentValidation;
using MovieLibrary.DTO;

namespace MovieLibrary.Validations
{
    public class AccountValidation : AbstractValidator<AccountDTO>
    {
        public AccountValidation()
        {
            RuleFor(x => x.Username).NotEmpty().MaximumLength(20);
            RuleFor(x => x.Password).NotEmpty().MaximumLength(10);
        }
    }
}
