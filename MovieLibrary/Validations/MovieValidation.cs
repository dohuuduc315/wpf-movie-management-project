﻿using FluentValidation;
using MovieLibrary.DTO;

namespace MovieLibrary.Validations
{
    public class MovieValidation : AbstractValidator<MovieDTO>
    {
        public MovieValidation()
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(30);
            RuleFor(x => x.Director).NotEmpty().MaximumLength(30);
            RuleFor(x => x.Rating).NotEmpty();
        }
    }
}
