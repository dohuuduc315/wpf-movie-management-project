﻿using FluentValidation;
using MovieLibrary.DTO;

namespace MovieLibrary.Validations
{
    public class ReviewValidation : AbstractValidator<ReviewDTO>
    {
        public ReviewValidation()
        {
            RuleFor(x => x.Title).NotEmpty();
            RuleFor(x => x.Content).NotEmpty();
            RuleFor(x => x.Rating).NotEmpty();
            RuleFor(x => x.MovieId).NotEmpty();
        }
    }
}
