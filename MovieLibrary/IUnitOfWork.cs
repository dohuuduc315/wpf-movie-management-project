﻿using MovieLibrary.IRepositories;

namespace MovieLibrary
{
    public interface IUnitOfWork : IDisposable
    {
        public IMovieRepository MovieRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        public IAccountRepository AccountRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
