﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.DTO
{
    public class ReviewDTO
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public double Rating { get; set; }
        public int MovieId { get; set; }
    }
}
