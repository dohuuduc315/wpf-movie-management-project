﻿namespace MovieLibrary.DTO
{
    public class MovieDTO
    {
        public string Name { get; set; }
        public string Director { get; set; }
        public double Rating { get; set; }
        public string Description { get; set; }
    }
}
