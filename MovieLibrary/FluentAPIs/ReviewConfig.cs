﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieLibrary.Entities;

namespace MovieLibrary.FluentAPIs
{
    public class ReviewConfig : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Movie)
                   .WithMany(x => x.Reviews)
                   .HasForeignKey(x => x.MovieId);
        }
    }
}
