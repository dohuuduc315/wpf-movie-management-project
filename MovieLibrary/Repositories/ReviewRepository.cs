﻿using Microsoft.EntityFrameworkCore;
using MovieLibrary.Entities;
using MovieLibrary.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.Repositories
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        public ReviewRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<Review>> GetReviewByTitleAsync(string title)
        {
            var review = await _dbSet.Where(x => x.Title.Contains(title))
                              .OrderBy(x => x.Id)
                              .ToListAsync();
            return review;
        }
    }
}
