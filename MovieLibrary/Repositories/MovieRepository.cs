﻿using Microsoft.EntityFrameworkCore;
using MovieLibrary.Entities;
using MovieLibrary.IRepositories;

namespace MovieLibrary.Repositories
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        public MovieRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<Movie>> GetMovieByNameAsync(string name)
        {
            var movie = await _dbSet.Where(x => x.Name.Contains(name))
                              .OrderBy(x => x.Id)
                              .ToListAsync();
            return movie;
        }
    }
}
