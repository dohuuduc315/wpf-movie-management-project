﻿using Microsoft.EntityFrameworkCore;
using MovieLibrary.Entities;
using MovieLibrary.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.Repositories
{
    public class AccountRepository : GenericRepository<Account>, IAccountRepository
    {
        public AccountRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<Account> GetAccountAsync(string username, string password)
        {
            var account = await _dbSet.Where(x => x.Username == username && x.Password == password)
                                      .FirstOrDefaultAsync();
            return account;
        }

        public async Task<Account> GetAccountByUsernameAsync(string username)
        {
            var account = await _dbSet.Where(x => x.Username == username).FirstOrDefaultAsync();
            return account;
        }
    }
}
