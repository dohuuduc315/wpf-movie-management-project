﻿namespace MovieLibrary.Entities
{
    public class Movie : BaseEntity
    {
        public string Name { get; set; }
        public string Director { get; set; }
        public double Rating { get; set; }
        public string Description { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }
}
