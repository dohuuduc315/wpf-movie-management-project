﻿namespace MovieLibrary.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
