﻿using AutoMapper;
using MovieLibrary.DTO;
using MovieLibrary.Entities;
using MovieLibrary.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public AccountService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<AccountDTO> AddAccountAync(AccountDTO accountDTO)
        {
            try
            {
                var accountObj = _mapper.Map<Account>(accountDTO);
                await _unitOfWork.AccountRepository.AddAsync(accountObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<AccountDTO>(accountObj);
                }
                return null;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<Account> GetAccount(string username, string password)
        {
            var account = await _unitOfWork.AccountRepository.GetAccountAsync(username, password);
            return account;
        }

        public async Task<Account> GetAccountByUsername(string username)
        {
            var account = await _unitOfWork.AccountRepository.GetAccountByUsernameAsync(username);
            return account;
        }
    }
}
