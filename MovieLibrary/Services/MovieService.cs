﻿using AutoMapper;
using MovieLibrary.DTO;
using MovieLibrary.Entities;
using MovieLibrary.IServices;

namespace MovieLibrary.Services
{
    public class MovieService : IMovieService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public MovieService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<MovieDTO> AddMovieAsync(MovieDTO movieDTO)
        {
            try
            {
                var movieObj = _mapper.Map<Movie>(movieDTO);
                await _unitOfWork.MovieRepository.AddAsync(movieObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return _mapper.Map<MovieDTO>(movieObj);
                }
                return null;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task DeleteMovieAsync(int id)
        {
            try
            {
                var movieObj = await _unitOfWork.MovieRepository.GetByIdAsync(id);
                if (movieObj is not null)
                {
                    _unitOfWork.MovieRepository.Remove(movieObj);
                    await _unitOfWork.SaveChangeAsync();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Movie>> GetAllMovies() => await _unitOfWork.MovieRepository.GetAllAsync();

        public async Task<Movie> GetMovieByIdAsync(int id) => await _unitOfWork.MovieRepository.GetByIdAsync(id);

        public async Task<List<Movie>> GetMoviesByName(string name) => await _unitOfWork.MovieRepository.GetMovieByNameAsync(name);

        public async Task<MovieDTO> UpdateMovieAsync(int id, MovieDTO movieDTO)
        {
            try
            {
                var movieObj = await _unitOfWork.MovieRepository.GetByIdAsync(id);
                if (movieObj is not null)
                {
                    _mapper.Map(movieDTO, movieObj);
                    _unitOfWork.MovieRepository.Update(movieObj);
                    var isSuccess = await _unitOfWork.SaveChangeAsync();
                    if (isSuccess > 0)
                    {
                        return _mapper.Map<MovieDTO>(movieObj);
                    }
                    return null;
                }
                else
                {
                    throw new Exception("The movie does not already exist.");
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
