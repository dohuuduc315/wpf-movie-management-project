﻿using AutoMapper;
using MovieLibrary.DTO;
using MovieLibrary.Entities;
using MovieLibrary.IServices;

namespace MovieLibrary.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ReviewService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<ReviewDTO> AddReviewAsync(ReviewDTO reviewDTO)
        {
            try
            {
                var reviewObj = _mapper.Map<Review>(reviewDTO);
                await _unitOfWork.ReviewRepository.AddAsync(reviewObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    _mapper.Map<ReviewDTO>(reviewObj);
                }
                return null;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task DeleteReviewAsync(int id)
        {
            try
            {
                var reviewObj = await _unitOfWork.ReviewRepository.GetByIdAsync(id);
                if (reviewObj is not null)
                {
                    _unitOfWork.ReviewRepository.Remove(reviewObj);
                    await _unitOfWork.SaveChangeAsync();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            
        }

        public async Task<List<Review>> GetAllReviewsAsync() => await _unitOfWork.ReviewRepository.GetAllAsync();

        public async Task<Review> GetReviewByIdAsync(int id) => await _unitOfWork.ReviewRepository.GetByIdAsync(id);

        public async Task<List<Review>> GetReviewsByTitle(string title) => await _unitOfWork.ReviewRepository.GetReviewByTitleAsync(title);

        public async Task<ReviewDTO> UpdateReviewAsync(int id, ReviewDTO reviewDTO)
        {
            try
            {
                var reviewObj = await _unitOfWork.ReviewRepository.GetByIdAsync(id);
                if(reviewObj is not null)
                {
                    _mapper.Map(reviewDTO, reviewObj);
                    _unitOfWork.ReviewRepository.Update(reviewObj);
                    var isSuccess = await _unitOfWork.SaveChangeAsync();
                    if (isSuccess > 0)
                    {
                        return _mapper.Map<ReviewDTO>(reviewObj);
                    }
                    return null;
                }
                else
                {
                    throw new Exception("The review does not already exist.");
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
