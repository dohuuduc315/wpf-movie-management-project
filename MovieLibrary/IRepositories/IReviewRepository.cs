﻿using MovieLibrary.Entities;

namespace MovieLibrary.IRepositories
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
        public Task<List<Review>> GetReviewByTitleAsync(string title);
    }
}
