﻿using MovieLibrary.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.IRepositories
{
    public interface IAccountRepository : IGenericRepository<Account>
    {
        Task<Account> GetAccountAsync(string username, string password);
        Task<Account> GetAccountByUsernameAsync(string username);
    }
}
