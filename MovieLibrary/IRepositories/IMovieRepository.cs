﻿using MovieLibrary.Entities;

namespace MovieLibrary.IRepositories
{
    public interface IMovieRepository : IGenericRepository<Movie>
    {
        public Task<List<Movie>> GetMovieByNameAsync(string name);
    }
}
