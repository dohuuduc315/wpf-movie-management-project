﻿using AutoMapper;
using MovieLibrary.DTO;
using MovieLibrary.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLibrary.Mappers
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<MovieDTO, Movie>().ReverseMap();
            CreateMap<ReviewDTO, Review>().ReverseMap();
            CreateMap<AccountDTO, Account>().ReverseMap();
        }
    }
}
